<html ng-app>
    <head>
        <title>INDT - Avaliação</title>

        <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ url('css/style.css') }}">

        <script src="{{ url('js/angular.min.js') }}"></script>

        <script src="{{ url('js/jquery.min.js') }}"></script>

        {{--<script src="{{ url('js/jquery-3.2.1.slim.min.js') }}"></script>--}}
        <script src="{{ url('js/popper.min.js') }}"></script>
        <script src="{{ url('js/bootstrap.min.js') }}"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="/">INDT</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarColor01">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/addauthor">Novo Autor <span class="sr-only">(current)</span></a>
                    </li>
                </ul>
                {{--<form class="form-inline">--}}
                    {{--<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">--}}
                    {{--<button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>--}}
                {{--</form>--}}
            </div>
        </nav>

        <div class="container">
            @yield('content')
        </div>


        <input type="text" ng-model="name"/>
        <h2>
            @verbatim
                {{name}}
            @endverbatim
        </h2>
    </body>
</html>
