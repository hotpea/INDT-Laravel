@extends('index')

@section('content')
    <form class="form-inline" action="/saveauthor" method="post">
        <div class="form-group mx-sm-3 mb-2">
            <input type="text" name="nome" class="form-control" placeholder="Nome">
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <input type="text" name="sobrenome" class="form-control" placeholder="Sobrenome">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Salvar</button>
    </form>
@endsection
