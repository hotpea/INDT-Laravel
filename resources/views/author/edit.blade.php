@extends('index')

@section('content')
    <form class="form-inline" action="/saveauthor" method="post">
        <input type="hidden" name="id" class="form-control" placeholder="id" value="{{ $author['id'] }}">

        <div class="form-group mx-sm-3 mb-2">
            <input type="text" name="nome" class="form-control" placeholder="Nome" value="{{ $author['firstName'] }}">
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <input type="text" name="sobrenome" class="form-control" placeholder="Sobrenome" value="{{ $author['lastName'] }}">
        </div>
        <button type="submit" class="btn btn-primary mb-2">Salvar</button>
    </form>
@endsection
