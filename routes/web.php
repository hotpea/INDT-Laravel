<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'authors',
    'uses' => 'AuthorController@getAllAuthors'
]);

// Author
Route::get('/authors/{page}', [
    'as' => 'authors',
    'uses' => 'AuthorController@getAllAuthors'
]);
Route::get('/addauthor', function () use ($router) {
    return view('author.add');
});

Route::get('/editauthor/{id}', [
    'as' => 'authors',
    'uses' => 'AuthorController@editAuthor'
]);

Route::get('/deleteauthor/{id}', [
    'as' => 'authors',
    'uses' => 'AuthorController@deleteAuthor'
]);

Route::post('/saveauthor', [
    'as' => 'authors',
    'uses' => 'AuthorController@save'
]);

// Books
Route::get('/books', [
    'as' => 'books',
    'uses' => 'BookController@getAllBooks'
]);

Route::get('/addbook/{authorid}', function ($authorid) use ($router) {
    return view('book.add', ['authorid' => $authorid ]);
});

Route::get('/editbook/{id}', [
    'as' => 'books',
    'uses' => 'BookController@editBook'
]);

Route::get('/deletebook/{id}', [
    'as' => 'books',
    'uses' => 'BookController@deleteBook'
]);

Route::post('/savebook', [
    'as' => 'books',
    'uses' => 'BookController@save'
]);
